package com.fiture.projectuas;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;


import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

public class DetailActivity extends AppCompatActivity {


    private TextView TampilDetail ;
    Bitmap bitMapImg;
    ImageView img;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        String SumberBerita = getIntent().getStringExtra("SourceBerita");
        setTitle(SumberBerita);



        TampilDetail = (TextView) findViewById(R.id.TampilNama);
        String DeskripsiBerita = getIntent().getStringExtra("DescriptionBerita");
        TampilDetail.setText(DeskripsiBerita);



        //menamilkan gambar
        img = (ImageView)findViewById(R.id.gambarBerita);
        String url = getIntent().getStringExtra("SumberGambar");

        new GetImageFromURL(img).execute(url);

    }

    public class GetImageFromURL extends AsyncTask<String, Void, Bitmap>{
        ImageView imgV;
        public GetImageFromURL(ImageView imgV){
            this.imgV = imgV;
        }
        @Override
        protected Bitmap doInBackground(String... url){
            String urlDisplay = url[0];
            bitMapImg = null;

            try{
                InputStream  srt = new java.net.URL(urlDisplay).openStream();
                bitMapImg = BitmapFactory.decodeStream(srt);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitMapImg;
        }

        protected void onPostExecute(Bitmap bitmap){
            super.onPostExecute(bitmap);
            imgV.setImageBitmap(bitmap);
        }
    }
}

